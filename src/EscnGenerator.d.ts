export declare class EscnFactory {
    static async sleep(): any;
    static async getEscn(prefix: number, pic: string): any;
    static getNode(intPrefix: number, pic: string): any;
}