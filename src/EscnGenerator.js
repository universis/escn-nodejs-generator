class EscnFactory {
    // Offset from 15 Oct 1582 to 1 Jan 1970
    static OFFSET_MILLIS = 12219292800000;
    static time = 0;
    static oldSysTime = 0;
    static clock = 0;
    static hits = 0;

    static async sleep() {
        return new Promise((resolve) => {
            void setTimeout(() => {
                const sysTime = Date.now();
                return resolve(sysTime);  
            }, 1);
        });
    }

    /**
    * This method calculate an UUID from 2 parameters
    * @param prefix to distinguish servers of a same institution
    * @param pic Participant Identification Code
    * @return a unique ESCN
    */
    static async getEscn(prefix, pic) {
        const node = EscnFactory.getNode(prefix, pic);

        if (--this.hits > 0) {
            ++this.time;
        } else {
            let sysTime = Date.now();
            this.hits = 10000;

            if (sysTime <= this.oldSysTime) {
                if (sysTime < this.oldSysTime) {
                    this.clock = (++this.clock & 0x3fff) | 0x8000;
                } else {
                    sysTime = await EscnFactory.sleep();
                }
            }
            this.time = sysTime * 10000 + this.OFFSET_MILLIS;
            this.oldSysTime = sysTime;
        }

        const low = this.time.toString(16);
        const mid = (this.time >> 32 & 0xffff).toString(16);
        const hi = ((this.time >> 48 & 0x0fff) | 0x1000).toString(16); 
        const escn = `${low.padStart(8, '0').slice(-8)}-${mid.padStart(4, '0').slice(-4)}-${hi.padStart(4, '0').slice(-4)}-${this.clock.toString(16).padStart(4, '0').slice(-4)}-${node}`;
        return escn.toLowerCase();
    }

    /**
     * This method calculates a node used for the ESCN creation and initiates the clock
     * node = Prefix + PIC
     *
     * @param intPrefix Used to distinguish servers of a same institution
     * @param pic Participant Identification Code
     * @return node
     * @throws "Invalid Prefix format"  if the prefix is malformed
     * @throws "Invalid PIC format"  if the PIC is malformed
     */    
    static getNode(intPrefix, pic) {
        if (intPrefix > 999 || intPrefix < 0) {
            throw new Error("Invalid Prefix format!");
        }

        const prefix = String(intPrefix).padStart(3, '0').slice(-3);

        if (!prefix.match(new RegExp("[0-9]{3}"))) {
            throw new Error("Invalid Prefix format!");
        }     

        if (!pic.match(new RegExp("[0-9]{9}"))) {
            throw new Error("Invalid PIC format!");
        }

        const node = prefix + pic;

        // 14 bit clock, set high 2 bits to '0001' for RFC 4122 variant 2
        this.clock = Math.floor(Math.random() * 0x3fff) | 0x8000;

        return node;
    }
}

export {
    EscnFactory
}
