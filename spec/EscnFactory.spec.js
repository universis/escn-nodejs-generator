import { EscnFactory } from '../src/EscnGenerator';
describe('EscnFactory', () => {
    it('should create escn', async () => {
        const result = await EscnFactory.getEscn(0, '123456789');
        expect(result).toBeTruthy();
    });
});