# @universis/escn-nodejs-generator

European Student Card Number generator for node.js

### Install

    npm i @universis/escn-nodejs-generator

### Usage

Use `EscnFactory.getEscn(prefix, pic)` to generate a new European Student Card Number

    import { EscnFactory } from '@universis/escn-nodejs-generator';

    const result = EscnFactory.getEscn(1, '123456789')

where `prefix` is a number which identifies a server instance and `pic` a string which represents a student identifier e.g. 123400000

